<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Session;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $coleccion = Producto::orderBy('nombre', 'asc')
            ->get();

        $request->session()->put('orden', null);
        $request->session()->put('nombre', null);
        $request->session()->put('precio_min', null);
        $request->session()->put('precio_max', null);

        return view('welcome')
            ->with('coleccion', $coleccion);
    }

    public function Filtro(Request $request)
    {
        $query = DB::table('productos');

        $validator = Validator::make($request->all(), [
            'orden' => 'nullable|in:asc,desc',
            'nombre' => 'nullable|string',
            'precio_min' => 'nullable|numeric',
            'precio_max' => 'nullable|numeric',
        ]);

        if ($this->sePuedeAsignar($request->orden) && $request->orden == 'desc') {
            $query = $query->orderBy('nombre', 'desc');
        } else {
            $query = $query->orderBy('nombre', 'asc');
        }

        if ($this->sePuedeAsignar($request->orden)) {
            $request->session()->put('orden', $request->orden);
        } else {
            $request->session()->put('orden', null);
        }

        if ($this->sePuedeAsignar($request->nombre)) {
            $query = $query->where('nombre', 'LIKE', '%' . $request->nombre . '%');
            $request->session()->put('nombre', $request->nombre);
        } else {
            $request->session()->put('nombre', null);
        }

        if ($this->sePuedeAsignar($request->precio_min)) {
            $query = $query->where('precio', '>=', $request->precio_min);
            $request->session()->put('precio_min', $request->precio_min);
        } else {
            $request->session()->put('precio_min', null);
        }

        if ($this->sePuedeAsignar($request->precio_max)) {
            $query = $query->where('precio', '<=', $request->precio_max);
            $request->session()->put('precio_max', $request->precio_max);
        } else {
            $request->session()->put('precio_max', null);
        }

        $coleccion = $query->get();

        return view('welcome')
            ->with('coleccion', $coleccion);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'precio' => 'required|numeric',
            'descripcion' => 'required|string',
        ]);

        if ($validator->fails()) {
            Session::flash('alert-class', 'alert-warning');
            Session::flash('mensaje', 'Hubo un error, favor de checar los campos');

            return redirect()
                ->route('productos.index')
                ->withInput()
                ->withErrors($validator);
        }

        $objeto = new Producto;
        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->precio = filter_var($request->precio, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $objeto->descripcion = filter_var($request->descripcion, FILTER_SANITIZE_STRING);;
        $objeto->save();

        Session::flash('alert-class', 'alert-success');
        Session::flash('mensaje', '¡Se inserto el producto de forma correcta!');

        return redirect()->route('productos.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Producto $producto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Producto $producto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Producto $producto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $objeto = Producto::find($id);
        $objeto->delete();

        Session::flash('alert-class', 'alert-danger');
        Session::flash('mensaje', '¡Se elimino el producto de forma correcta!');

        return redirect()->route('productos.index');
    }

    public function sePuedeAsignar($objeto)
    {
        $res = false;

        if (isset($objeto) && !empty($objeto)) {
            $res = true;
        }

        return $res;
    }
}
