<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Producto;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < 50; $i++) {
            Producto::create([
                'nombre' => "Producto $faker->word",
                'descripcion' => $faker->paragraph,
                'precio' => $faker->randomFloat(2, 10, 100),
            ]);
        }
    }
}
