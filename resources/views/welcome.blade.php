<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.2/css/dataTables.bootstrap4.min.css">
    <title>Ejercicio Tecnico</title>
</head>

<body>
    <section>
        <div class="container-fluid">
            <div class="container">
                <div class="row my-3">
                    <h1 class="text-center">Listado de Productos</h1>
                    @if (Session::has('mensaje'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible fade show" role="alert">
                            <h4>{{ Session::get('mensaje') }}</h3>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <div class="d-flex justify-content-between">
                        <div class="col-md-3 my-3">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#Registrar">
                                Registrar producto
                            </button>
                        </div>
                        <div class="col-md-3 my-3 mr-0">
                            @if ($coleccion->count())
                                <h5>Total productos: {{ $coleccion->count() }}</h5>
                            @endif
                        </div>
                    </div>
                    <form method="POST" action="{{ route('filtro') }}">
                        {{ csrf_field() }}
                        <div class="row d-flex align-items-center">
                            <div class="col-md-2 my-1">
                                <label class="form-label">Ordenar</label>
                                <select class="form-select" name="orden" aria-label="Default select example">
                                    <option disabled selected>Orden alfabeticamente por nombre</option>
                                    <option value="asc" {{ session::get('orden') == 'asc' ? 'selected' : null }}>Ascendente</option>
                                    <option value="desc" {{ session::get('orden') == 'desc' ? 'selected' : null }}>Descendente</option>
                                </select>
                            </div>
                            <div class="col-md-3 my-1">
                                <label for="nombre" class="form-label">Nombre</label>
                                <input type="search" class="form-control" id="nombre" name="nombre" placeholder="Nombre del producto" value="{{ session::get('nombre') ? session::get('nombre') : null }}">
                            </div>
                            <div class="col-md-3 my-1 text-center">
                                <label class="form-label">Rango de precio</label>
                                <div class="row">
                                    <div class="col-md-6 my-1">
                                        <input type="number" min="0" step="0.01" class="form-control" name="precio_min" placeholder="minino" value="{{ session::get('precio_min') ? session::get('precio_min') : null }}">
                                    </div>
                                    <div class="col-md-6 my-1">
                                        <input type="number" min="0" step="0.01" class="form-control" name="precio_max" placeholder="maximo" value="{{ session::get('precio_max') ? session::get('precio_max') : null }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 my-1 d-flex justify-content-end">
                                <button type="submit" class="btn btn-info">Filtrar busqueda</button>
                            </div>
                            <div class="col-md-2  my-1 d-flex justify-content-end">
                                <a class="btn btn-secondary" href="{{ route('productos.index') }}">Limpiar busqueda</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table id="datatable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Accion</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($coleccion->count())
                                    @foreach ($coleccion as $objeto)
                                        <tr>
                                            <th scope="row" width="20%">{{ $objeto->nombre }}</th>
                                            <td class="text-center" width="15%">${{ number_format($objeto->precio, 2) }}</td>
                                            <td>{{ $objeto->descripcion }}</td>
                                            <td width="10%">
                                                <form method="POST" action="{{ route('productos.destroy', [$objeto->id]) }}">
                                                    {{ csrf_field() }}
                                                    @method('DELETE')
                                                    <button class="btn btn-danger">Eliminar</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <h4 class="text-center">
                                                Actualmente no tienes ningun producto registrado
                                                {{ session::get('nombre') ? 'que contenga ' . '"' . session::get('nombre') . '"' : null }}
                                                {{ session::get('nombre') && session::get('precio_min') ? 'y ' : null }}
                                                {{ session::get('precio_min') ? 'que sea mayor de ' . '"$' . number_format(session::get('precio_min'), 2) . '"' : null }}
                                                {{ session::get('precio_min') && session::get('precio_max') ? 'y ' : null }}
                                                {{ session::get('precio_max') ? 'que sea menor de ' . '"$' . number_format(session::get('precio_max'), 2) . '"' : null }}
                                            </h4>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Agregar-->
        <div class="modal fade" id="Registrar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Registrar Nuevo Producto</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="POST" action="{{ route('productos.store') }}">
                        {{ csrf_field() }}
                        <div class="row modal-body">
                            <div class="col-md-6">
                                <label for="nombre" class="form-label">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del producto" value="{{ old('nombre') }}" required>
                                @if ($errors->has('nombre'))
                                    <span class="help-block text-danger">
                                        {{ $errors->first('nombre') }}
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label for="precio" class="form-label">Precio</label>
                                <input type="number" min="0" step="0.01" class="form-control" id="precio" name="precio" placeholder="Precio del producto" value="{{ old('precio') }}" required>
                            </div>
                            <div class="col-md-12 my-3">
                                <label for="descripcion" class="form-label">Descripcion</label>
                                <textarea class="form-control" id="descripcion" name="descripcion" cols="30" rows="5" placeholder="Descripcion del producto" required>{{ old('descripcion') }}</textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>
