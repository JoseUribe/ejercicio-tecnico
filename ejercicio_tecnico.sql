-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 25-02-2023 a las 22:21:51
-- Versión del servidor: 5.7.34
-- Versión de PHP: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ejercicio_tecnico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_02_25_174429_create_productos_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` double NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `precio`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Producto doloribus', 43.52, 'Dolores assumenda eos amet veniam. Sed provident deserunt id quia est. Voluptate facere officiis et accusamus qui eaque. Eos accusantium perspiciatis qui quam.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(2, 'Producto ut', 58.44, 'Rerum veniam perspiciatis labore. Aut autem minus nam earum quas nihil et dolor. Autem perspiciatis qui repudiandae voluptates eos autem. Sint est vitae voluptatem tempore non. Blanditiis distinctio sit corporis consequatur.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(3, 'Producto cumque', 35.77, 'Consequatur ut et commodi consequuntur. Eaque quisquam ullam dolorem quos architecto. Ut ut sapiente nostrum iste debitis aut corrupti. Aut sapiente officia dolorem harum.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(4, 'Producto consequatur', 41.29, 'Officiis totam in qui. Doloremque eius a maxime dicta laudantium molestias eaque eos. Rem incidunt nostrum eius. Facilis eum dolorem beatae vero et aut. Fuga quidem tempora sit nisi et.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(5, 'Producto repudiandae', 11.35, 'Quasi sed nihil et dolore ex. In natus sed reprehenderit est. Est natus nesciunt molestiae aliquam aut et.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(6, 'Producto quod', 21.8, 'Culpa dicta voluptatem amet cumque dicta eligendi. Et itaque nesciunt quia sed sint pariatur et. Cupiditate aut aliquam facere voluptate expedita provident consequuntur.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(7, 'Producto nulla', 22.97, 'Est omnis aut dolor voluptate. Esse et esse ut necessitatibus. Id veritatis similique incidunt repellat dolorum natus consequuntur.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(8, 'Producto quis', 56.46, 'Amet est voluptas fuga qui et ut. Unde impedit excepturi culpa quia qui sunt cupiditate qui. Mollitia laudantium omnis veniam.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(9, 'Producto rerum', 16.08, 'Beatae quae rerum sit aut. Praesentium facilis repellat quisquam soluta veniam. Eius nobis aliquam et est. Quos et eligendi sint dolor adipisci magni dolorem.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(10, 'Producto ut', 38.94, 'Reprehenderit et quis dicta qui repellendus assumenda. Optio dolores repellendus aspernatur odio praesentium officiis. Enim sit ipsum corporis officia et alias.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(11, 'Producto nostrum', 89.27, 'Esse est illum maxime et. Aut perspiciatis id repellat. Et distinctio tenetur temporibus officiis. Consequatur corporis dignissimos reprehenderit quo.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(12, 'Producto corporis', 47.19, 'Voluptas fugiat commodi fugiat repellendus corporis. Veritatis ducimus dolorem sint repellendus architecto sit odit sunt. Est totam omnis sapiente magni eum ex repellendus voluptas.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(13, 'Producto nihil', 17.33, 'Non dolorem hic alias sunt necessitatibus est sint. Omnis aliquid voluptatem non dolorem commodi reprehenderit necessitatibus. Animi maxime ipsam quod quidem tenetur sunt numquam. Ipsam sed totam reprehenderit sed dolore voluptatem temporibus natus.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(14, 'Producto qui', 39.03, 'Consequatur nulla sapiente quo aut. Cum vel molestiae vel corrupti enim. Omnis ut est quisquam officiis omnis officiis.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(15, 'Producto inventore', 56.42, 'Unde quasi laborum numquam quia. Eveniet ipsam fuga est sed magni repellendus nobis. Voluptatem velit possimus aliquid minus cupiditate molestias.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(16, 'Producto pariatur', 58.02, 'Vel et ut harum velit sequi. Eligendi ut saepe doloremque et vel animi facilis. Reiciendis doloribus non blanditiis mollitia. Est commodi non consequatur provident.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(17, 'Producto eius', 55.78, 'Velit minus et voluptas ea voluptatibus accusamus. Aliquam ipsa fugiat ipsam voluptatem ut. Qui et laborum ratione architecto possimus ad quo. Et aliquam voluptas veniam alias facilis aut.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(18, 'Producto eos', 87.96, 'Iste illum ducimus eum necessitatibus ut amet quisquam non. Laboriosam molestiae magnam dicta harum quidem. Aut dolores numquam maiores sed eius et architecto. Qui et blanditiis est autem.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(19, 'Producto distinctio', 91.26, 'Magni assumenda blanditiis eligendi in sed facilis. Aliquid alias itaque quae similique exercitationem. Officia voluptatibus quia tempore aliquam atque blanditiis vero.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(20, 'Producto quaerat', 77.95, 'Non sapiente doloremque qui officiis autem excepturi. Repellendus iusto at vitae doloremque error dolorum. Quasi quia alias ut earum et.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(21, 'Producto quo', 62.35, 'Et ut eos perferendis voluptas beatae nobis velit. Vel sint nulla quia vitae ut fugit laboriosam. Sequi in omnis qui non. Enim mollitia nihil consectetur eum distinctio et voluptatum.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(22, 'Producto blanditiis', 64.82, 'Voluptatem qui animi rerum voluptatem et voluptas. Aut adipisci ipsa ut placeat fuga iste. Perspiciatis magni et quae accusamus et.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(23, 'Producto quod', 65.69, 'Labore voluptas pariatur rerum omnis eum incidunt. Necessitatibus assumenda sed odit vero iste velit.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(24, 'Producto doloremque', 24.51, 'Quia enim nihil rerum alias odio. Et commodi consequatur cupiditate harum explicabo dolorem illo. Ratione magnam in et sed in est. Et ut dolor voluptatibus et ipsam distinctio ducimus.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(25, 'Producto consequuntur', 96.12, 'Porro nesciunt expedita saepe quas voluptates ipsa. In officiis unde aut ut est minus voluptatem odio.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(27, 'Producto sit', 14.72, 'Omnis quidem voluptates non optio voluptatum dolore tempore. Aperiam ratione possimus quos aut sint repellendus deserunt. Id reprehenderit nobis enim qui et qui fugit quia. Ab quo et ipsam est.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(28, 'Producto error', 58.33, 'Quia sit illo molestiae quas. Rerum suscipit qui nobis voluptates et. Eum possimus voluptate in quas ex accusamus. Cum mollitia quas corrupti est.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(29, 'Producto molestiae', 36.94, 'Cumque ex aut et quae illum est voluptatem. Vel ratione dolorum exercitationem est. Et et mollitia voluptatum veritatis.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(30, 'Producto et', 77.84, 'Beatae magni non repellat voluptatum non nesciunt placeat. Aspernatur alias non nam dolores nesciunt. Corrupti illo delectus iure velit sint.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(31, 'Producto non', 84.1, 'Cumque ut maiores fuga vero enim mollitia accusantium saepe. Ut molestias nam quia fugiat ducimus non. Perferendis in facilis et sit fugit est qui. Praesentium fuga sit saepe voluptatibus facilis dicta reprehenderit.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(32, 'Producto saepe', 20.02, 'Neque voluptate ut veritatis quidem tempore ea quidem. Sit quisquam temporibus quisquam qui laboriosam voluptas. Aut ipsum porro quae sed. Aut qui quam ad qui.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(33, 'Producto doloremque', 53.16, 'Autem perferendis et mollitia eos facere. Eaque reprehenderit minus architecto at. Dolorem dolor non occaecati sint distinctio iste. Explicabo dolorum deserunt consectetur voluptas recusandae.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(34, 'Producto sint', 61.41, 'Nobis aut provident consequatur dolores unde. Consequuntur omnis odio maxime et. Repellat quia accusamus enim rerum modi adipisci tempora. Voluptatibus ea perspiciatis qui aliquid deleniti et.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(35, 'Producto impedit', 33.34, 'Rerum totam laboriosam quam. Ut porro rerum alias non. Veniam rerum eligendi quisquam.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(36, 'Producto sequi', 55.16, 'Fuga accusantium laborum fugiat earum ipsam eligendi omnis. Dolorem laborum hic mollitia ullam est. Accusamus incidunt tenetur fugit.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(37, 'Producto repellendus', 99.08, 'Repudiandae est recusandae error quia velit quo ullam. Officiis hic odit qui sed iste. Eum sit voluptatem dignissimos optio nihil rerum voluptates.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(38, 'Producto aliquam', 37.15, 'Magni reiciendis soluta dolores deleniti. Eveniet eius est cumque debitis.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(39, 'Producto quas', 67.2, 'Dolore accusamus aut sit quas id necessitatibus. Animi repellendus velit et quaerat eius.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(40, 'Producto sapiente', 86.68, 'Culpa iusto animi sint est. Distinctio ut odit nisi optio corrupti adipisci et similique. Vitae numquam ipsum est voluptatibus.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(41, 'Producto ipsum', 91.33, 'Fuga rem animi aut at error minus omnis ipsam. Dolorum sequi ducimus id corrupti. Dolorum dolores amet rerum nihil voluptates nisi. Commodi quasi est eum eveniet modi ea soluta et.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(42, 'Producto incidunt', 41.08, 'Eos dolore quam iste quia incidunt. Impedit est harum voluptatem autem consequatur non nostrum aut. Repellendus sit aut et quisquam. Quod aut amet optio neque.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(43, 'Producto ipsum', 15.1, 'Quia rerum quis dolor ut accusantium itaque. In optio quia ducimus voluptas sed. Sint officia nulla nam sed qui vel. Nisi cupiditate voluptatum eos fugiat non consectetur.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(44, 'Producto quam', 78.28, 'Autem laboriosam quaerat blanditiis nemo sed eligendi. Eum necessitatibus sint enim. Est animi sit accusantium accusantium eveniet.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(45, 'Producto aut', 95.27, 'Ut voluptas praesentium qui totam corporis occaecati consectetur. Quibusdam consequatur molestias est eum distinctio amet. Odio consequuntur vitae ipsum perferendis nulla cumque.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(46, 'Producto facilis', 20.52, 'Veniam consequatur labore dolores voluptas qui sit laborum. Est ratione eum inventore similique. Totam quae voluptas suscipit sint vel quam ut. Ratione quasi dolores sequi ea.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(47, 'Producto qui', 11.23, 'Numquam odio nemo soluta odio. Nemo corporis voluptas voluptas qui ut est. Eum incidunt eos ab at. Ad quia assumenda non sed eaque est dolorum. Distinctio eius quaerat enim nobis at explicabo ipsam.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(48, 'Producto ducimus', 86.74, 'Aut et voluptatibus minima. Sit molestiae voluptas non sit sunt. Dolorem hic qui dignissimos in eveniet modi et. Est officiis odio nisi molestiae nostrum. Tempore accusantium dolor ipsum sed aut aperiam.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(49, 'Producto sint', 28.39, 'Numquam perspiciatis ab aliquid maxime necessitatibus architecto. Modi sit vitae est aut commodi adipisci quia dicta. Nam ut dignissimos dolorum animi hic quia quam sunt. Est doloribus nulla aut repudiandae.', '2023-02-26 04:08:42', '2023-02-26 04:08:42'),
(50, 'Producto quo', 83.99, 'Error blanditiis exercitationem et. Omnis ipsa sunt magnam deserunt velit a non. Aut illo maiores quia soluta voluptatem eos culpa. Ut sint praesentium et quia recusandae velit rem. Vitae sequi ea nulla porro natus commodi.', '2023-02-26 04:08:42', '2023-02-26 04:08:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
